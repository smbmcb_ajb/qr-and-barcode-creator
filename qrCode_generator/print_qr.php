<?php
    include '../main_function/login.php';  
    if(!isset($_SERVER['HTTP_REFERER'])){
        // redirect them to your desired location
        header('location: ../index.php');
        exit;
    }
    session_start();

    if(!isset($_SESSION['EmpNum'])){
        header("Location:../index.php");
    }

    if(isset($_POST["SIGNOUT"])){
        session_destroy();
        unset($_SESSION["EmpNum"]);
        unset($_SESSION["FullName"]);
        header("Location: ../index.php");
        
        $_SERVER['HTTP_REFERER'] = "";
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>QR Code Page</title>
    <link rel="stylesheet" href="assets/css/main/app.css">
    <link rel="stylesheet" href="assets/css/main/app-dark.css">
    <link rel="stylesheet" href="./css/print_qr.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.1/css/jquery.dataTables.min.css">
    <script>
    /*to prevent Firefox FOUC, this must be here*/
    let FF_FOUC_FIX;
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
    <script src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>
    <script src="https://kit.fontawesome.com/51460444e6.js" crossorigin="anonymous"></script>
    <!-- <script src="https://kit.fontawesome.com/7ea5798589.js" crossorigin="anonymous"></script> -->

    <!-- FAVICON -->
    <link rel="shortcut icon" type="image/jpg" href="../favicon_io/android-chrome-512x512.png"/>
</head>
<body style="background-color: #CCCDCF;">

<header>
    <nav>
        <div id="navbar" class="vertical_center" style="background-color: #fff; height:60px;">
            <div class="logo">Warehouse Receiving</div>
        </div>
        <div class="nav-links" style="background-color: #EBEBEB; height:50px;padding-left:30px; ">
            <button class="nav-btn" style="color: #B8B8B8; font-weight: 600;" onclick=mainPage()>Receiving</button>
            <form action="" method="post">
                <button name="SIGNOUT" class="nav-btn logout-btn">Logout<i class="fa-solid fa-power-off" style="margin-left: 5px;"></i></button>
            </form>
            <span style="margin-left:auto; margin-right:10px; font-weight: bold; color: #3c5393">
                <?php echo $_SESSION['FullName']; 
                    
                ?>
                <div class="text-center" style="font-size: 10px;">
                    <?php 
                        if($_SESSION['Admin'] == true){
                            echo "ADMIN";
                        }elseif($_SESSION['Admin'] == false){
                            echo "STAFF";
                        }
                    ?>
                </div>
            </span>
            <span class="rounded-circle" style="margin-right: 30px; border: 2px solid #3c5393;width:40px;height:40px;display:flex; align-items:center;justify-content:center;color: #3c5393;">
                <i class="fa-solid fa-user-tie fa-xl"></i>
            </span>
        </div>
    </nav>
</header>
<div class="container-fluid text-center mx-auto" >
    <div class="row mt-3 w-100 mx-auto" style="background-color: #CCCDCF; ">
        <div class="row mx-auto mb-3" >
            <div id="table-data" class="col-sm-12 mt-4 mb-3" style="overflow-x: auto; ">
                <table id="data-table" class="table table-bordered mb-3 mt-5" style="background-color:white;min-width: 600px;width:100%; box-shadow: 0px 3px 10px #888888;">
                    <thead>
                        <tr style="background-color: #3C5393; color:white;">
                            <th class="text-center">PO</th>
                            <th class="text-center">GOODS CODE</th>
                            <th class="text-center" style="max-width: 70px;">ITEM CODE</th>
                            <th class="text-center">ASSY LINE</th>
                            <th class="text-center">DATE RECEIVE</th>
                            <th class="text-center" style="max-width: 70px;">INVOICE</th>
                            <th class="text-center">QUANTITY</th>
                            <th class="text-center" style="word-break:normal;max-width: 50px;">NUMBER OF BOX</th>
                            <th class="text-center" style="word-break:normal;max-width: 80px;">BOX LABEL QR</th>
                            <th class="text-center" style="word-break:normal;max-width: 80px;">INNER LABEL QR</th>
                            
                        </tr>
                    </thead>
                    
                    <!-- <tbody id="dataTable"></tbody> -->
                    <tfoot>
                        <tr><td colspan="10">
                            <button class="btn btn-success " id="printQR" style="">
                                <span class="fa-solid fa-print fa-xl" style="vertical-align: middle; position:relative;">
                                    <span id="qrTotalPieces" style="position:absolute;top:-25px;left:15px;font-size:14px;" class="badge badge-light"></span>
                                </span>
                            </button>
                            <button class="btn btn-success " id="printQRFloat" style="">
                                <span class="fa-solid fa-print fa-xl" style="vertical-align: middle; position:relative;">
                                    <span id="qrTotalPiecesFloat" style="position:absolute;top:-25px;left:15px;font-size:14px;" class="badge badge-light"></span>
                                </span>
                            </button>
                        </td></tr>
                    </tfoot>
                </table>
                
                <center>
                    <div id="spinnerBar" class="spinner-border mt-5" style="width: 3rem; height: 3rem;" role="status">
                        <span class="visually-hidden">Loading...</span>
                        
                    </div>
                </center>
                </div>
            </div>
            
        </div>
    </div>
</div>
<!-- <div id="testContainer"></div> -->
<script>
    // window.addEventListener('scroll', function(){
    //     console.log(document.getElementById("printQR"))
    // })

    let mainPage = ()=>{
        location.href = "../main_page.php"
    }
    var aData = [];
    // console.log("sadfasd")
    let search = ()=> {
    // document.getElementById('dataTable').innerHTML = "";
    // document.getElementById('table-data').style.display = "none"
    // document.getElementById('spinnerBar').style.display = "block"
    // if(document.getElementById('part_number').value !== ""){
        $(document).ready(function () {
            fetch('./api/qr_details.php', {
            method: 'POST',
            headers: {
                'Content-Type':'application/json'
            },
            // body: JSON.stringify({
            //     "data": document.getElementById('part_number').value
            // })
            })

            .then(res => {
                
                return res.json()
            })
            .then(result => {
                // checkDuplicate(result)
                // console.log(result[0].BOXLABEL_CHECKBOX)
                document.getElementById('table-data').style.display = "block"
                document.getElementById('spinnerBar').style.display = "none"
                var table = $('#data-table').DataTable({
                    "order": [[ 4, "desc" ]],
                    select: true,
                    data: result,
                    columns: [
                        { data: "PO" },
                        { data: "GOODS_CODE" },
                        { data: "ITEM_CODE" },
                        { data: "ASY_LINE" },
                        { data: "DATE_RECEIVE" },
                        { data: "INVOICE" },
                        { data: "QTY" },
                        { data: "NUMBER_OF_BOX" },
                        <?php if($_SESSION['Admin'] == false){ ?>
                        { data: "isPrinted",
                            defaultContent: 
                            `<div class='form-check' style='display:flex;justify-content:center;align-items:center;padding-left:0;'><input style='margin-left:0;' class='form-check-input boxLabelQR' type='checkbox' value='' name='boxLabelQR'></div>`,
                            orderable: false,
                            render: function(data,type,row){
                                // console.log(type)
                                if(data == 1){
                                    // console.log("tested")
                                    return `<span class='badge badge-pill badge-success'>Printed by ${row['BOX_QR_PRINTED_BY']}</span>`
                                }
                            },
                        },
                        { data: "INNER_QR_LABEL",
                            defaultContent: 
                            `<div class='form-check' style='display:flex;justify-content:center;align-items:center;padding-left:0;'><input style='margin-left:0;' class='form-check-input innerLabelQR' type='checkbox' value='' name='innerLabelQR'></div>`,
                            orderable: false,
                            render: function(data,type,row){
                                if(data == 1){
                                    // console.log("tested")
                                    return `<span class='badge badge-pill badge-success'>Printed by ${row['INNER_QR_PRINTED_BY']}</span>`
                                }
                            },
                        },
                        <?php }elseif($_SESSION['Admin'] == true){ ?>
                        { data: null,
                            defaultContent: 
                            `<div class='form-check' style='display:flex;justify-content:center;align-items:center;padding-left:0;'><input style='margin-left:0;' class='form-check-input boxLabelQR' type='checkbox' value='' name='boxLabelQR'></div>`,
                        },
                        { data: null,
                            defaultContent: 
                            `<div class='form-check' style='display:flex;justify-content:center;align-items:center;padding-left:0;'><input style='margin-left:0;' class='form-check-input innerLabelQR' type='checkbox' value='' name='innerLabelQR'></div>`,
                        },
                        <?php } ?>
                    ],
                    
                });
               
            })
        });
    }
            

setTimeout(()=>{
    $(document).ready(function(){

        

    });
    
},1000)
let samplePrint = ()=>{

}

window.onload = search();


    setTimeout(()=>{
        $(document).ready(function(){
            // document.writeln("res")
            // $('.btnDemo').on('click', function(){
            //     if($(".btnDemo").is(":checked")){
            //         console.log('hjdfhjdshfjad')
            //         }
                
            // })
            var aData = [];
            var innerLabelData = [];
            // $('#data-table tbody').on('click', 'td', function () {
            //     console.log($(this).prop)
            // })
            $('#data-table tbody').on('click', 'input', function () {
                
                // console.log(document.getElementById("tblQRContainer").innerText)
                var index
                // var aData = [];
                var table = $('#data-table').DataTable();
                // var data = table.row(this).data();
                var data = table.row( $(this).parents('tr') ).data();
                    fetch('./api/get_material_code.php', {
                    method: 'POST',
                    headers: {
                        'Content-Type':'application/json'
                    },
                    body: JSON.stringify({
                        "data": data.GOODS_CODE
                    })
                    })

                    .then(res => {
                        return res.json()
                    })
                    .then(result => {
                        // console.log(result)
                        data.MATERIAL_CODE = result.MATERIAL_CODE
                        // console.log(data.GOODS_CODE)       
                    })

                    // fetch('./api/get_spq.php', {
                    //     method: 'POST',
                    //     headers: {
                    //         'Content-Type':'application/json'
                    //     },
                    //     body: JSON.stringify({
                    //         "data": data.GOODS_CODE.replace("-","")
                    //     })
                    //     })
                    //     .then(res => {
                    //         return res.json()
                    //     })
                    //     .then(result => {
                    //         data.SPQ = result.SPQ
                    //         // console.log(result)
                    //         if(data.QTY % data.SPQ != 0){
                    //             data.NumberOfInnerQR = Math.trunc(data.QTY / data.SPQ) + 1
                    //         }else{
                    //             data.NumberOfInnerQR = data.QTY / data.SPQ
                    //         }
                    //         // console.log()
                    //     })
                    if(data.QTY % data.SPQ != 0){
                            data.NumberOfInnerQR = Math.trunc(data.QTY / data.SPQ) + 1
                        }else{
                            data.NumberOfInnerQR = data.QTY / data.SPQ
                        }
                // currentRow = $(this)
                // var data = table.row( $(this).parents('tr') ).data();
                let aDataStringified = JSON.stringify(data);
                if($(this).prop("checked") == true){
                    if($(this).prop("name") == "boxLabelQR"){
                        jQuery("#table-data .innerLabelQR").prop("disabled",true);
                        
                        // console.log(document.getElementsByClassName(['boxLabelQR']:checked)
                        // console.log(data)
                        // console.log($(this).prop("name"))
                        
                        $('body').append(`<div style='display:none;' id='${data.ID}'></div>`)
                        qrc = new QRCode(document.getElementById(data.ID), data.PO+";"+data.GOODS_CODE+";"+data.ITEM_CODE+";"+data.INVOICE);
                        aData.push(data)
                        document.getElementById('qrTotalPieces').textContent = Number(document.getElementById('qrTotalPieces').textContent) + Number(data.NUMBER_OF_BOX)
                        document.getElementById('qrTotalPiecesFloat').textContent = Number(document.getElementById('qrTotalPiecesFloat').textContent) + Number(data.NUMBER_OF_BOX)
                    }
                    if($(this).prop("name") == "innerLabelQR"){
                        jQuery("#table-data .boxLabelQR").prop("disabled",true);
                        
                        // console.log(document.getElementsByClassName(['boxLabelQR']:checked)
                        // console.log(data)
                        // console.log($(this).prop("name"))
                        $('body').append(`<div style='display:none;' id='${data.ID}'></div>`)
                        $('body').append(`<div id="${'barcodeContainer'+data.ID}" style='display:none;justify-content:center;align-items:center; width: 180px;height:30px;'><svg id="${'barcode'+data.ID}" style='width: 180px;height:30px;'></svg></div>`)
                        qrc = new QRCode(document.getElementById(data.ID), data.GOODS_CODE);
                        
                        JsBarcode(`#${"barcode"+data.ID}`, data.ITEM_CODE,{
                            // format: "pharmacode",
                            width:1,
                            height:20,
                            displayValue: false,
                            background: "none"
                        });
                        
                        innerLabelData.push(data)

                        document.getElementById('qrTotalPieces').textContent = Number(document.getElementById('qrTotalPieces').textContent) + Number(data.NumberOfInnerQR)
                        document.getElementById('qrTotalPiecesFloat').textContent = Number(document.getElementById('qrTotalPiecesFloat').textContent) + Number(data.NumberOfInnerQR)

                        // let tempData
                        // fetch('./api/get_spq.php', {
                        // method: 'POST',
                        // headers: {
                        //     'Content-Type':'application/json'
                        // },
                        // body: JSON.stringify({
                        //     "data": data.GOODS_CODE.replace("-","")
                        // })
                        // })
                        // .then(res => {
                        //     return res.json()
                        // })
                        // .then(result => {
                            
                        //     tempData = data
                        //     tempData.SPQ = result.SPQ
                        //     // console.log(result)
                        //     if(tempData.QTY % tempData.SPQ != 0){
                        //         tempData.NumberOfInnerQR = Math.trunc(tempData.QTY / tempData.SPQ) + 1
                        //     }else{
                        //         tempData.NumberOfInnerQR = tempData.QTY / tempData.SPQ
                        //     }
                        //     // console.log()
                        // }).then(result => {
                        //     console.log(tempData.NumberOfInnerQR)
                        //     document.getElementById('qrTotalPieces').textContent = Number(document.getElementById('qrTotalPieces').textContent) + Number(tempData.NumberOfInnerQR)
                        //     document.getElementById('qrTotalPiecesFloat').textContent = Number(document.getElementById('qrTotalPiecesFloat').textContent) + Number(tempData.NumberOfInnerQR)
                        // })
                    }
                    
                }else{
                    if($(this).prop("name") == "boxLabelQR"){
                        document.getElementById('qrTotalPieces').textContent = Number(document.getElementById('qrTotalPieces').textContent) - Number(data.NUMBER_OF_BOX)
                        document.getElementById('qrTotalPiecesFloat').textContent = Number(document.getElementById('qrTotalPiecesFloat').textContent) - Number(data.NUMBER_OF_BOX)
                        if(document.getElementById('qrTotalPieces').textContent == 0){
                            document.getElementById('qrTotalPieces').textContent = ""
                        }
                        if(document.getElementById('qrTotalPiecesFloat').textContent == 0){
                            document.getElementById('qrTotalPiecesFloat').textContent = ""
                        }
                        if($(".boxLabelQR:checked").length == 0){
                            jQuery("#table-data .innerLabelQR").prop("disabled",false);
                        }
                        // document.getElementById(data.ID).innerHTML = ""
                        document.getElementById(data.ID).remove();
                        aData.map((item)=>{
                        // console.log(item.ID)
                            if(data.ID == item.ID){
                                // console.log(item)
                                index = aData.findIndex(el => el.ID == item.ID)
                                aData.splice(index, 1);
                                // console.log(aData.indexOf(item.ID))
                            }
                        })
                        
                    }
                    // console.log(localStorage.getItem('totalQRPieces'))

                    if($(this).prop("name") == "innerLabelQR"){
                        // document.getElementById('qrTotalPieces').textContent = Number(document.getElementById('qrTotalPieces').textContent) - Number(data.NumberOfInnerQR)
                        if($(".innerLabelQR:checked").length == 0){
                            jQuery("#table-data .boxLabelQR").prop("disabled",false);
                        }
                        document.getElementById(data.ID).remove();
                        // document.getElementById("barcode"+data.ID).remove();
                        document.getElementById("barcodeContainer"+data.ID).remove();
                        innerLabelData.map((item)=>{
                        // console.log(item.ID)
                            if(data.ID == item.ID){
                                // console.log(item)
                                index = innerLabelData.findIndex(el => el.ID == item.ID)
                                innerLabelData.splice(index, 1);
                                // console.log(innerLabelData.indexOf(item.ID))
                            }
                        })

                        // let tempData
                        // fetch('./api/get_spq.php', {
                        // method: 'POST',
                        // headers: {
                        //     'Content-Type':'application/json'
                        // },
                        // body: JSON.stringify({
                        //     "data": data.GOODS_CODE.replace("-","")
                        // })
                        // })
                        // .then(res => {
                        //     return res.json()
                        // })
                        // .then(result => {
                            
                        //     tempData = data
                        //     tempData.SPQ = result.SPQ
                        //     // console.log(result)
                        //     if(tempData.QTY % tempData.SPQ != 0){
                        //         tempData.NumberOfInnerQR = Math.trunc(tempData.QTY / tempData.SPQ) + 1
                        //     }else{
                        //         tempData.NumberOfInnerQR = tempData.QTY / tempData.SPQ
                        //     }
                        //     // console.log()
                        // }).then(result => {
                        //     console.log(tempData.NumberOfInnerQR)
                        //     document.getElementById('qrTotalPieces').textContent = Number(document.getElementById('qrTotalPieces').textContent) - Number(tempData.NumberOfInnerQR)
                        //     document.getElementById('qrTotalPiecesFloat').textContent = Number(document.getElementById('qrTotalPiecesFloat').textContent) - Number(tempData.NumberOfInnerQR)

                        //     if(document.getElementById('qrTotalPieces').textContent == 0)
                        //     {
                        //         document.getElementById('qrTotalPieces').textContent = ""
                        //     }
                        //     if(document.getElementById('qrTotalPiecesFloat').textContent == 0)
                        //     {
                        //         document.getElementById('qrTotalPiecesFloat').textContent = ""
                        //     }
                        // })
                        document.getElementById('qrTotalPieces').textContent = Number(document.getElementById('qrTotalPieces').textContent) - Number(data.NumberOfInnerQR)
                            document.getElementById('qrTotalPiecesFloat').textContent = Number(document.getElementById('qrTotalPiecesFloat').textContent) - Number(data.NumberOfInnerQR)

                            if(document.getElementById('qrTotalPieces').textContent == 0)
                            {
                                document.getElementById('qrTotalPieces').textContent = ""
                            }
                            if(document.getElementById('qrTotalPiecesFloat').textContent == 0)
                            {
                                document.getElementById('qrTotalPiecesFloat').textContent = ""
                            }
                    }
                    
                }
                // console.log(aData)
                // console.log(innerLabelData)
                

            })
            

            $('#printQR,#printQRFloat').on('click', function(){
                return Swal.fire({
                title: 'Are you sure you want to print these QR(s)?',
                // text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3c5393',
                cancelButtonColor: '#d33',
                confirmButtonText: "Yes, I'm sure!"
                }).then((result) => {
                    // console.log(result.isConfirmed)
                    if (result.isConfirmed === false) {
                        return
                    }
                    // console.log($(".boxLabelQR:checked").length)
                    // return console.log($(".innerLabelQR:checked").length)
                    if($(".boxLabelQR:checked").length > 0){
                        // createBoxLabelQR()
                        var a = window.open('', '', 'height=auto, width=auto, margin=0');
                        a.document.write('<html>');
                        a.document.write('<head>');
                        a.document.write('<link rel="stylesheet" href="css/print_design.css" />')
                        
                        a.document.write('</head>');
                        a.document.write("<body>");
                        // console.log(aData)
                        for(let i=0; i<aData.length; i++){
                            // console.log(aData[i].DATE_RECEIVE)
                            // console.log(typeof(aData[i].DATE_RECEIVE.split("-")[1]))
                            let monthData = aData[i].DATE_RECEIVE.split("-")[1]
                            // bgColor(monthData)
                            // console.log(bgColor(monthData))
                            updateBoxQR_printStatus(aData[i].ID)
                            // console.log(updateBoxQR_printStatus(aData[i].ID).MESSAGE)
                            // document.getElementById("qrcode-for-print").innerHTML = ""
                            // qrc = new QRCode(document.getElementById("qrcode-for-print"), aData[i].PO+";"+aData[i].GOODS_CODE+";"+aData[i].ITEM_CODE+";"+aData[i].INVOICE);
                            // console.log(aData)
                            // console.log(makeQR(aData[i].PO, aData[i].GOODS_CODE, aData[i].ITEM_CODE, aData[i].INVOICE))
                            // console.log(aData[i].NUMBER_OF_BOX)
                            for(let x=0; x<aData[i].NUMBER_OF_BOX; x++){
                                a.document.write("<div style='display:inline-block;'>");
                                a.document.write("<div style='max-width:259px; display:flex; flex-direction:row; height: 110px; '>");
                                    a.document.write(`<div id='first-row-to-print' style='width:130px;border:1px solid black; background-color:${bgColor(monthData).bgColor}; color:${bgColor(monthData).color}'>`);
                                    // console.log(bgColor().bgColor)
                                        a.document.write("<div style='height: 28px;display:flex; flex-direction:row;'>")
                                            a.document.write("<div style='height: 28px;width:65px; border-right:1px solid black;display:flex;justify-content:center; align-items:center;'>")
                                                a.document.write(x + 1);
                                            a.document.write("</div>");
                                            a.document.write("<div style='height: 28px;width:65px;border-left:1px solid black;display:flex;justify-content:center; align-items:center;'>")
                                                a.document.write(aData[i].NUMBER_OF_BOX);
                                            a.document.write("</div>")
                                        a.document.write("</div>")
                                        a.document.write("<div style='height: 27px;border-top:1px solid black;border-bottom:0;display:flex;justify-content:center; align-items:center;'>")
                                            a.document.write(aData[i].INVOICE);
                                        a.document.write("</div>")
                                        a.document.write("<div style='height: 27px;border-top:1px solid black;border-bottom:0;display:flex;justify-content:center; align-items:center;'>")
                                            a.document.write(aData[i].DATE_RECEIVE);
                                        a.document.write("</div>")
                                        a.document.write("<div style='height: 28px;border-top:1px solid black;display:flex;justify-content:center; align-items:center; font-weight:bold'>")
                                            a.document.write(aData[i].MATERIAL_CODE);
                                        a.document.write("</div>")
                                    a.document.write("</div>")
                                    // a.document.write("<div style='width:130px;border: 1px solid red;'>");

                                    // a.document.write("</div>")
                                    a.document.write("<div style='width:130px;border: 1px solid black;display:flex;'>");
                                        a.document.write("<div style='width:65px;height: 55px;display:flex;margin-top: 7px; margin-left: 2px;'>");
                                            a.document.write(document.getElementById(aData[i].ID).innerHTML);
                                            
                                        a.document.write("</div>")
                                    a.document.write("</div>")
                                a.document.write("</div>")
                                a.document.write("</div>");
                            }
                        }
                        
                        a.document.write('</body></html>');
                        // setTimeout(()=>{
                            
                        // }, 2000)
                        
                        // 
                        setTimeout(()=>{
                            a.print();
                            // console.log(a.onunload)
                            a.close()
                            location.reload(); 
                            Swal.fire({
                            icon: 'success',
                            // title: 'Hooray',
                            text: 'QR code is being printed!',
                            })
                        }, 1000)
                    }
                    // console.log($(".innerLabelQR:checked").length)
                    // console.log($(".boxLabelQR:checked").length)
                    if($(".innerLabelQR:checked").length > 0){
                        // console.log("inner label printing")
                        var b = window.open('', '', 'height=auto, width=auto, margin=0');
                        b.document.write('<html>');
                        b.document.write('<head>');
                        b.document.write('<link rel="stylesheet" href="css/print_design.css" />')
                        
                        b.document.write('</head>');
                        b.document.write("<body>");
                        
                        // console.log(innerLabelData[i].NumberOfInnerQR)
                        for(let i=0; i < innerLabelData.length; i++){
                            
                            let monthData = innerLabelData[i].DATE_RECEIVE.split("-")[1]
                            updateInnerQR_printStatus(innerLabelData[i].ID)
                            for(let x=0; x<innerLabelData[i].NumberOfInnerQR; x++){
                                b.document.write("<div style='display:inline-block;font-size:10px; font-weight:bold;'>");
                                    b.document.write("<div style='width:192px; display:flex; flex-direction:column; height: 96px;border: 1px solid black;margin-top:1px; '>");
                                        b.document.write("<div style='display:flex; flex-direction:row;width: 100%;height:64px;'>")
                                            b.document.write("<div style='width: 64px;height:64px;'>")
                                                b.document.write("<div style='width: 64px;display:flex;justify-content:center;align-items:center;height:21.3px'>")
                                                    b.document.write(innerLabelData[i].MATERIAL_CODE)
                                                b.document.write("</div>")
                                                b.document.write("<div id='item_code' style='width: 64px;display:flex;justify-content:center;align-items:center;height:21.3px;'>")
                                                    b.document.write(innerLabelData[i].ITEM_CODE)
                                                    
                                                    // console.log(innerLabelData[i].ITEM_CODE.length)
                                                b.document.write("</div>")
                                                b.document.write("<div style='width: 64px;display:flex;justify-content:center;align-items:center;height:21.3px;'>")
                                                    b.document.write(innerLabelData[i].GOODS_CODE)
                                                b.document.write("</div>")
                                            b.document.write("</div>")
                                            b.document.write("<div style='width: 64px;height:64px;'>")
                                                b.document.write("<div style='width: 64px;display:flex;justify-content:center;align-items:center;height:21.3px'>")
                                                    b.document.write(innerLabelData[i].INVOICE)
                                                b.document.write("</div>")
                                                b.document.write("<div style='width: 64px;display:flex;justify-content:center;align-items:center;height:21.3px;'>")
                                                    b.document.write(`<div style='width: 55px;height:18px;background-color:${bgColor(monthData).bgColor}'>`)
                                                    b.document.write("</div>")
                                                    // console.log(innerLabelData[i].ITEM_CODE.length)
                                                b.document.write("</div>")
                                                b.document.write("<div style='width: 64px;display:flex;justify-content:center;align-items:center;height:21.3px;'>")
                                                    b.document.write((x+1)+"/"+innerLabelData[i].NumberOfInnerQR)
                                                b.document.write("</div>")
                                            b.document.write("</div>")
                                            b.document.write("<div style='width: 64px;display:flex;justify-content:center;align-items:center;height:64px;'>")
                                                b.document.write("<div style='width: 52px;display:flex;height:52px;'>")
                                                    b.document.write(document.getElementById(innerLabelData[i].ID).innerHTML);
                                                b.document.write("</div>")
                                            b.document.write("</div>")
                                        b.document.write("</div>")
                                        b.document.write("<div style='width:100%; height:32px;display:flex;justify-content:center;align-items:center;'>")
                                            b.document.write("<div style='width: 180px;height:30px;display:flex;justify-content:center;align-items:center;'  >")
                                                b.document.write(document.getElementById("barcodeContainer"+innerLabelData[i].ID).innerHTML)
                                                // b.document.write("text")
                                            b.document.write("</div>")
                                        b.document.write("</div>")
                                        
                                    b.document.write("</div>")
                                b.document.write("</div>");
                            }
                        }
                        // console.log(document.getElementById('item_code'))

                        b.document.write('</body></html>');
                        setTimeout(()=>{
                            b.print();
                            // console.log(b)
                            b.close()
                            location.reload(); 
                            Swal.fire({
                            icon: 'success',
                            // title: 'Hooray',
                            text: 'QR code is being printed!',
                            })
                        }, 1000)
                    }
                    
                })
                
                
            });
        })
    },1000)

    let createBoxLabelQR = ()=>{
        
    }

let updateBoxQR_printStatus = (dataID)=>{
    return fetch('./api/update_print_status.php', {
    method: 'POST',
    headers: {
        'Content-Type':'application/json'
    },
    body: JSON.stringify({
        "data": dataID
    })
    })

    .then(res => {
        
        return res.json()
    })
    .then(result => { 
        return result
    })
}

let updateInnerQR_printStatus = (dataID)=>{
    return fetch('./api/update_innerQR_print_status.php', {
    method: 'POST',
    headers: {
        'Content-Type':'application/json'
    },
    body: JSON.stringify({
        "data": dataID
    })
    })

    .then(res => {
        
        return res.json()
    })
    .then(result => { 
        return result
    })
}
// bgColor('01')
var bgColor = (month)=>{
    // console.log(month)
    if(month == '01'){
        return {"bgColor":"#0000FF",
                "color": "white"
                }
    }
    else if(month == '02'){
        return {"bgColor":"#8F00FF",
                "color": "white"
                }
    }
    else if(month == '03'){
        return {"bgColor":"#F47F39",
                "color": "black"
                }
    }
    else if(month == '04'){
        return {"bgColor":"#A2B2AC",
                "color": "white"
                }
    }
    else if(month == '05'){
        return {"bgColor":"#4C9A2A",
                "color": "white"
                }
    }
    else if(month == '06'){
        return {"bgColor":"#0D0C12",
                "color": "white"
                }
    }
    else if(month == '07'){
        return {"bgColor":"#FFC0CB",
                "color": "black"
                }
    }
    else if(month == '08'){
        return {"bgColor":"#964B00",
                "color": "white"
                }
    }
    else if(month == '09'){
        return {"bgColor":"#FED758",
                "color": "black"
                }
    }
    else if(month == '10'){
        return {"bgColor":"#7AD7F0",
                "color": "black"
                }
    }
    else if(month == '11'){
        return {"bgColor":"#FFFDFA",
                "color": "black"
                }
    }
    else{
        return {"bgColor":"#880808",
                "color": "white"
                }
    }
// document.getElementById("color-container").textContent = ""
// document.getElementById("goodsCode-container").textContent = document.getElementById('goods_code').textContent
        
}
// window.addEventListener('afterprint', (event) => {
//   alert('After print');
// });
// window.addEventListener('afterprint', (event) => { alert("printed") });
// checkDuplicate();
// const checkDuplicate = (nums) => {
//     // console.log(nums)
// //   nums.sort(); // alters original array
//   let ans = []

//   for(let i = 0; i< nums.length; i++){
//     for(let x = 0; x < nums.length; x++){
//         if(nums[i].PO === nums[x].PO && nums[i].GOODS_CODE === nums[x].GOODS_CODE && nums[i].ITEM_CODE === nums[x].ITEM_CODE && nums[i].INVOICE === nums[x].INVOICE && nums[i].DATE_RECEIVE === nums[x].DATE_RECEIVE){
//             // console.log(nums[i].PO)
//             console.log('duplicate')
//         }
//     }
//   }
// }

let getSPQ = ()=>{
    fetch('./api/get_spq.php', {
    method: 'POST',
    headers: {
        'Content-Type':'application/json'
    },
    body: JSON.stringify({
        "data": "AM000003"
    })
    })
    .then(res => {
        return res.json()
    })
    .then((result)=>{
        // console.log(result)
    })
}   

window.addEventListener('scroll', function(e){
    var printQRButton = document.getElementById('printQR')
    var printQRButtonFloat = document.getElementById('printQRFloat')
    var bounding = printQRButton.getBoundingClientRect();

    if (
	bounding.top >= 0 &&
	bounding.left >= 0 &&
	bounding.right <= (window.innerWidth || document.documentElement.clientWidth) &&
	bounding.bottom <= (window.innerHeight + 30 || document.documentElement.clientHeight + 30)
    )
    {
        // console.log('In the viewport!');
        // printQRButtonFloat.style.bottom = "-60px"
        printQRButtonFloat.style.right = "-100px"
        // printQRButtonFloat.style.display = "none"
    } else {
        // console.log('Not in the viewport... whomp whomp');
        // printQRButtonFloat.style.bottom = "10px"
        printQRButtonFloat.style.right = "10px"
        // printQRButtonFloat.style.display = "flex"
        
    }

})
</script>
<!-- barcode cdn -->
<script src="https://cdn.jsdelivr.net/npm/jsbarcode@3.11.0/dist/JsBarcode.all.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.rawgit.com/davidshimjs/qrcodejs/gh-pages/qrcode.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/qrcodejs/1.0.0/qrcode.min.js" integrity="sha512-CNgIRecGo7nphbeZ04Sc13ka07paqdeTu0WR1IM4kNcpmBAUSHSQX0FslNhTDadL4O5SAGapGt4FodqL8My0mA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="./js/print_qr.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>   
<script src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>
<script src="assets/js/bootstrap.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
</body>
</html>