<?php

include '../1Connection.php';

$currentYear = date('Y');
$currentMonth = date('m');
$currentDay = date('d');
$EndDayOfTheMonth = '';
$MonthName = date("F", mktime(0, 0, 0, $currentMonth, 10));

if ($currentMonth == "01" || 
    $currentMonth == "03" || 
    $currentMonth == "05" || 
    $currentMonth == "07" || 
    $currentMonth == "08" || 
    $currentMonth == "10" || 
    $currentMonth == "12") {

    $EndDayOfTheMonth = "31";

}

elseif( $currentMonth == "04" || 
        $currentMonth == "06" || 
        $currentMonth == "09" || 
        $currentMonth == "11") {

        $EndDayOfTheMonth = "30";

}

else {

    $EndDayOfTheMonth = "28";

}

$query = "SELECT * FROM [dbo].[Receive] 
WHERE ARCHIVE = '0'
AND INVOICE != 'STARTING' 
AND DATE_RECEIVE >= '$currentYear-$currentMonth-01'
AND DATE_RECEIVE <= '$currentYear-$currentMonth-$EndDayOfTheMonth'
ORDER BY DATE_RECEIVE DESC, id DESC";
$result = sqlsrv_query($conn, $query);

$counter = '0';

while($rows=sqlsrv_fetch_array($result)){
    $id[$counter] = $rows['id'];
    $p_slip[$counter] = $rows['P_SLIP'];
    $s_slip[$counter] = $rows['S_SLIP'];
    $goods_code[$counter] = $rows['GOODS_CODE'];
    $inv[$counter] = $rows['INVOICE'];
    $qty[$counter] = $rows['QTY'];
    $po[$counter] = $rows['PO'];
    $item_code[$counter] = $rows['ITEM_CODE'];
    $assy_line[$counter] = $rows['ASY_LINE'];
    $sup[$counter] = $rows['SUPPLIER'];
    $p_num[$counter] = $rows['PART_NUMBER'];
    $p_name[$counter] = $rows['PART_NAME'];
    $date_received[$counter] = $rows['DATE_RECEIVE']->format('Y-m-d');
    $date_received_m[$counter] = $rows['DATE_RECEIVE']->format('m');
    $status[$counter] = $rows['STATUS'];

    $counter = $counter + 1;
}

?>

<table id="report_table" class="text_center fixed" style="white-space: nowrap;">

    <thead>
        <tr>
            <th>PURCHASE SLIP</th>
            <th>SALES SLIP</th>
            <th>status</th>
            <th>goods code</th>
            <th>invoice</th>
            <th>quantity</th>
            <th>P.O</th>
            <th>item code</th>
            <th>assy line</th>
            <th>supplier</th>
            <th>part number</th>
            <th>part name</th>
            <th>date receive</th>
        </tr>
    </thead>

    <tbody>
            
        <?php
            for ($x = 0; $x <= $counter; $x++) {

                $color = "#fff";

                if ($date_received_m[$x] == '01'){ $bg = "#0000FF"; }
                elseif ($date_received_m[$x] == '02'){ $bg = "#8F00FF"; }
                elseif ($date_received_m[$x] == '03'){ $bg = "#F47F39"; }
                elseif ($date_received_m[$x] == '04'){ $bg = "#A2B2AC"; $color = "#000"; }
                elseif ($date_received_m[$x] == '05'){ $bg = "#4C9A2A"; }
                elseif ($date_received_m[$x] == '06'){ $bg = "#0D0C12"; }
                elseif ($date_received_m[$x] == '07'){ $bg = "#FFC0CB"; $color = "#000"; }
                elseif ($date_received_m[$x] == '08'){ $bg = "#964B00"; }
                elseif ($date_received_m[$x] == '09'){ $bg = "#FED758"; $color = "#000"; }
                elseif ($date_received_m[$x] == '10'){ $bg = "#7AD7F0"; $color = "#000"; }
                elseif ($date_received_m[$x] == '11'){ $bg = "#FFFDFA"; $color = "#000"; }
                else{$bg = "#880808";}

                if($id[$x] != ''){
                    echo "<tr style='color:".$color."; background:".$bg.";'>
                            <td>" . $p_slip[$x] . "</td>
                            <td>" . $s_slip[$x] . "</td>
                            <td>" . $status[$x] . "</td>
                            <td>" . $goods_code[$x] . "</td>
                            <td>" . $inv[$x] . "</td>
                            <td>" . number_format($qty[$x]) . "</td>
                            <td>" . $po[$x] . "</td>
                            <td>" . $item_code[$x] . "</td>
                            <td>" . $assy_line[$x] . "</td>
                            <td>" . $sup[$x] . "</td>
                            <td>" . $p_num[$x] . "</td>
                            <td>" . $p_name[$x] . "</td>
                            <td>" . $date_received[$x] . "</td>
                            
                        </tr>";
                }
            }
        ?>

    </tbody>

</table>


<!-- SCRIPT FOR DATATABLE -->
<script>
    $('#report_table').DataTable({ 
        "ordering": false,
        "scrollX": true,
        // "lengthChange": false, // disable the length 10/25/50/100
        "pagingType": "simple_numbers", //paging type numbers/simple/simple_numbers/full/full_numbers/first_last_numbers
        // "pageLength": 10,
        "searching": false
    });
</script>