<?php

include '../1Connection.php';
include '../main_function/login.php';

$currentYear = date('Y');
$currentMonth = date('m');
$currentDay = date('d');
$EndDayOfTheMonth = '';

$search_invoice = $_POST['search_invoice'] ?? '';

$query = "SELECT * FROM [dbo].[Receive] 
WHERE CHARINDEX ('0', ARCHIVE) > 0 
AND CHARINDEX ('$search_invoice', INVOICE) > 0
AND SLIP_EDIT IS NULL
ORDER BY DATE_RECEIVE DESC,
id DESC";
$result = sqlsrv_query($conn, $query);

echo "<table id='search_slip_data' class='table w3-table-all w3-hoverable ui striped table'>";

echo "<thead class='report_table_head'>
        <tr>
            <th>invoice</th>
            <th>p slip</th>
            <th>s slip</th>
            <th>goods code</th>
            <th>quantity</th>
            <th>item code</th>
        </tr>
      </thead>";

echo "<tbody class='report_table_body'>";




while($rows=sqlsrv_fetch_array($result)){

    $color = "#fff";
    // $color = "#000";

    if($rows['DATE_RECEIVE']->format('m') == '01'){
        $bg = "#0000FF";
        // $bg = "#5265B2";
    }
    elseif($rows['DATE_RECEIVE']->format('m') == '02'){
        $bg = "#8F00FF";
        // $bg = "#C199CE";
    }
    elseif($rows['DATE_RECEIVE']->format('m') == '03'){
        // $bg = "orange";
        $bg = "#F47F39";
    }
    elseif($rows['DATE_RECEIVE']->format('m') == '04'){
        // $bg = "gray";
        $bg = "#A2B2AC";
        $color = "#000";
    }
    elseif($rows['DATE_RECEIVE']->format('m') == '05'){
        $bg = "#4C9A2A";
        // $bg = "#7DB166";
    }
    elseif($rows['DATE_RECEIVE']->format('m') == '06'){
        // $bg = "black";
        $bg = "#0D0C12";
    }
    elseif($rows['DATE_RECEIVE']->format('m') == '07'){
        $bg = "#FFC0CB";
        $color = "#000";
        // $bg = "#E9B7C3";
        // $color = "#000";
    }
    elseif($rows['DATE_RECEIVE']->format('m') == '08'){
        $bg = "#964B00";
        // $bg = "#9E8748";
        // $color = "#000";
    }
    elseif($rows['DATE_RECEIVE']->format('m') == '09'){
        // $bg = "yellow";
        $bg = "#FED758";
        $color = "#000";
    }
    elseif($rows['DATE_RECEIVE']->format('m') == '10'){
        $bg = "#7AD7F0";
        // $bg = "#9AB9D6";
        $color = "#000";
    }
    elseif($rows['DATE_RECEIVE']->format('m') == '11'){
        // $bg = "#fff";
        $bg = "#FFFDFA";
        $color = "#000";
    }
    else{
        $bg = "#880808";
        // $bg = "#DB937B";
    }


    if($rows['STATUS'] == 'RAW'){
        $status = '#880808';
    }
    else{
        $status = 'green';
    }

                                            
    echo "<tr style='color:".$color."; background:".$bg.";'>
            <td>" . $rows['INVOICE'] . "</td>
            <td>" . $rows['P_SLIP'] . "</td>
            <td>" . $rows['S_SLIP'] . "</td>
            <td>" . $rows['GOODS_CODE'] . "</td>
            <td>" . $rows['QTY'] . "</td>
            <td>" . $rows['ITEM_CODE'] . "</td>
        </tr>";
}

echo "</tbody>

</table>";

?>