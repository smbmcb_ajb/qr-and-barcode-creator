<?php

include '1Connection.php';
include 'main_function/login.php';

if(!isset($_SERVER['HTTP_REFERER'])){
    // redirect them to your desired location
    header('location: index.php');
    exit;
}

if(!isset($_SESSION['EmpNum'])){
    header("Location:index.php");
}

if($_SERVER['REQUEST_METHOD'] == "POST" and isset($_POST['logout_desktop'])) {
    func();
}

function func()
{
    session_destroy();
    unset($_SESSION["EmpNum"]);
    unset($_SESSION["FullName"]);
    unset($_SESSION["Admin"]);
    header("Location: index.php");    
}

$EmpNum = $_SESSION['EmpNum'];
$Name = $_SESSION['FullName'];
$Admin = $_SESSION['Admin'];

if(isset($_POST["Logout"])){
    unset($_SESSION["EmpNum"]);
    unset($_SESSION["FullName"]);
    header("Location: index.php");
}

if(isset($_POST["SIGNOUT"])){
    unset($_SESSION["EmpNum"]);
    unset($_SESSION["FullName"]);
    header("Location: index.php");
}

$currentYear = date('Y');
$currentMonth = date('m');
$currentDay = date('d');
$EndDayOfTheMonth = '';

$MonthName = date("F", mktime(0, 0, 0, $currentMonth, 10));

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- <meta name="color-scheme" content="dark"> -->
    <title>Warehouse Main Page</title>

    <!-- XLSX -->
    <script src="https://cdn.jsdelivr.net/npm/xlsx@0.18.0/dist/xlsx.full.min.js"></script>
    

    <!-- EXTERNAL LINK FOR FONT AWESOME AND SEMANTIC UI-->  
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/1.11.8/semantic.min.css"/> 

    <!-- FONT AWESOME 5 -->
    <script src="https://kit.fontawesome.com/9b0df8280e.js" crossorigin="anonymous"></script>

    <!-- JQUERY / INSTASCAN / SEMANTIC UI / VUE JS / ANGULAR -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/1.11.8/semantic.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.14"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.8/angular.min.js"></script>


    <!-- JQUERY DATATABLE PLUGIN -->
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css" />

    <!-- SWAL BITCH -->
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <Script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.4.30/dist/sweetalert2.all.min.js"></Script>


    <script src="//cdn.rawgit.com/rainabba/jquery-table2excel/1.1.0/dist/jquery.table2excel.min.js"></script>


    <!-- HTML 5 QR SCANNER -->
    <!-- <script src="https://raw.githubusercontent.com/mebjas/html5-qrcode/master/minified/html5-qrcode.min.js"></script>
    <script src="https://unpkg.com/html5-qrcode" type="text/javascript"></script> -->

    <!-- EXTERNAL JS -->
    <script type="text/javascript" src="js/scan.js" async></script>
    <!-- <script type="text/javascript" src="js/torch.js" async></script> -->
    <script type="text/javascript" src="js/user_modal.js" async></script>

    <!-- EXTERNAL CSS -->
    <link rel="stylesheet" href="css/main_page.css" />
    <link rel="stylesheet" href="css/scan.css" />
    <link rel="stylesheet" href="css/global.css" />
    <link rel="stylesheet" href="css/ajax_table.css" />
    <link rel="stylesheet" href="css/user_modal.css" />
    <link rel="stylesheet" href="css/menu.css" />
    <link rel="stylesheet" href="css/report.css" />
    <link rel="stylesheet" href="css/slideshow.css" />
    <link rel="stylesheet" href="css/edit.css" />
    <link rel="stylesheet" href="css/import.css" />
    <link rel="stylesheet" href="css/report_table.css" />
    <link rel="stylesheet" href="css/ajax_user_desktop.css" />
    <link rel="stylesheet" href="css/search_result.css" />
    <link rel="stylesheet" href="css/datatables.css" />
    <link rel="stylesheet" href="css/incoming.css" />

    <!-- FAVICON -->
    <link rel="shortcut icon" type="image/jpg" href="favicon_io/android-chrome-512x512.png"/>
    
</head>

<body>



    <!-- SCANNING PART USING HTML5 QR SCANNER -->
    <!-- <div id="preview"></div>
    <div id="result"></div> -->
    
    <!-- NAVIGATION MENU -->
    <header>
        <nav>
            <div id="navbar" class="vertical_center">
                <div id="logo" class="reverse">
                    <div class="system_title">
                        Warehouse Receiving
                    </div>
                </div>
                
                <div class="user_div close">
                    <a href="#demo-modal">
                        <i class="fa-solid fa-circle-user"></i> <br> 
                    </a>
                </div>
                
            </div>

            <div id="links">
                <button class="nav_menu close" id="slip_edit">Slip Edit</button>
                <button class="nav_menu close" id="iqc_data">IQC Data</button>
                <button class="nav_menu" id="receive_desktop">Receiving</button>
                <button class="nav_menu" id="report_desktop">Reports</button>
                <button class="nav_menu" id="to_be_receive_desktop">Expected Materials</button>
                <button class="nav_menu" id="print_desktop">Print QR Code</button>
                <button class="nav_menu close" disabled id="import_desktop">Import</button>
                <button class="nav_menu close" id="download_desktop">Download</button>
                <form action="main_page.php" method="post">
                    <button name="SIGNOUT" class="nav_menu logout_button" id="logout_desktop" name="logout_desktop">Logout <i class="fa-solid fa-power-off"></i></button>
                </form>
            </div>
            
        </nav>

    </header>

    <!-- NEW USER MODAL -->
    <div class="modal">
        <div class="modal-content">
            <span class="close-button">×</span>

            <div class="img_container">
                <img src="https://wwbmmc.ca/wp-content/uploads/2020/12/kisspng-computer-icons-avatar-icon-design-male-teacher-5ade176c636ed2.2763610715245044284073.png" 
                alt="<?php echo $Name; ?>"> 
            </div>

            <div class="user_info">

                <div>
                    <span class="user_label">
                        Name: <span class="user_content"><?php echo $Name; ?></span>
                    </span>
                </div>
                <hr>
                <div>
                    <span class="user_label">
                        Employee Number:
                        <span class="user_content"><?php echo $EmpNum; ?></span>
                    </span>
                </div>

                
                
            </div>

            <form action="index.php" method="post">
                <button class="Logout" name="Logout">LOG OUT</button>
            </form>

        </div>
    </div>

    <!-- MENU FOR REPORT / RECEIVING / EXPORT -->
    <div class="menu_div container">
        <div class="Col-1-50 report_div">
            <button id="reports"><span id="report_text">Reports</span></button>
        </div>

        <div class="Col-2-50 download_div">
            <button id="receive" >Receiving</button>
        </div>
        
    </div>

    <!-- SCAN USING INSTASCAN -->
    <div class="QR-SCANNER-DIV close">

        <div class="container_in">

            <div class="Col-1-33_in">
                <div class="frame-container">
                    <p class="Label close">CAMERA AREA</p>
                    <video id="preview"></video>
                </div>
            </div>

            <div class="Col-2-33_in">

                <div class="container Button-Scan-Container">

                    <div class="Col-1-50 Div-Scan" style="margin-right:10px;">

                        <button class="Scan-Button"> 
                            <i class="fa-solid fa-qrcode close"></i>
                            <span class="button_text">SCAN QR</span>
                        </button>

                    </div>

                    <div class="Col-2-50 Div-Stop" style="margin-left:10px;">

                        <button class="Stop-Button"> 
                            <i class="fa-solid fa-square-xmark close"></i>
                            <span class="button_text">CLOSE</span>
                        </button>

                    </div>

                    <div class="Col-3-33 Div-Flash close">

                        <button class="FLash-Button"> 
                            <i class="fa-solid fa-lightbulb"></i> <br> 
                            <span class="button_text">FLASH</span>
                        </button>

                    </div>

                </div>

                <div class="form_part"> 
                    <div class="container">
                        <div class="Col-1-70">
                            <input type="text" value="" id="QR-Code" name="QR-Code" placeholder="Search P.O / QR">
                        </div>
                        <div class="Col-2-30">
                            <button id="search_item"><i class="fa-solid fa-magnifying-glass close"></i> SEARCH</button>
                            <!-- <button class="close" id="count_words"><i class="fa-solid fa-magnifying-glass close"></i> COUNT</button> -->
                        </div>
                    </div>
                </div>

                <div id="ajax_search_result"></div>

            </div>

            <div class="Col-3-33_in div_info_area">
                <div class="center close"><p class="info_area_text">Information Area</p></div>
                <div class="ajax_table_desktop" id="ajax_table"></div>
            </div>

        </div>

        <div class="container_in incoming_div">
            <div id="incoming_today"></div>
        </div>

        <div class="ajax_table_mobile" id="ajax_table"></div>

    </div>

    <!-- AJAX FOR DATA VIEWING OR REPORT -->
    <div class="ajax_report"></div>


    <!-- DIV PARA SA SLIDER NA MAY INSTRUCTION ON HOW TO USE MOBILE -->
    <div class="container" id="slide_show">
        <div class="frame">
            <div class="scroller">

                <div class="element" id="slide1">
                    <div class="slide_content">
                        <h1 class="title">WAREHOUSE RECEIVING SYSTEM</h1>
                        <p class="color_000">If you haven't been informed on how to use this app,<br> click the <b> "Learn More" </b> option. </p> 
                        <p class="color_000"> If you have any questions or concerns about this system, <br/> you can call the developer (<b>536</b>).</p> 
                        <span><button id="learn_more_desktop">Learn More <i class="fa-solid fa-arrow-right"></i></button></span>

                        <p style="margin-top:20px; font-weight:700; color:#880808;" class="close"> 
                            <i class="fa-solid fa-triangle-exclamation"></i>
                            Sorry for the inconvenience, but the "Import" option is now unavailable, with no indication on when it will be restored.
                        </p>
                    </div>
                </div>

                <div class="element" id="slide2">
                    
                        <h2 class="title_steps">MATERIAL RECEIVING</h2>

                        <img class="image_instruction" src="images/receiving_image/receiving.png" alt="">

                        <p class="color_fff">
                            <b>Step 1: </b>
                            To access the Receiving page, click the 
                            <b>"Receiving"</b>
                            button, which has a 
                            <b>"Warehouse"</b> 
                            icon.
                        </p> 

                        <hr>

                        <img class="image_instruction" src="images/receiving_image/receive_default.png" alt="">

                        <p class="color_fff">
                            The default page of <b>"Receiving"</b> comprises one input box for manually entering item codes or scanning them with a scanner, 
                            and three buttons: one for <b>"QR CODE"</b> icon, which opens the camera, 
                            one for <b>"X"</b> icon, which closes the camera, 
                            and one for <b>"BULB"</b> symbol, which opens the phone's flash.
                        </p> 

                        <hr>

                        <a href="#slide3"><button id="next">Next</button></a>
                    
                </div>

                <div class="element" id="slide3">
                    <p>Box 3</p>
                    <a href="#slide1"><button id="learn_more">Complete <i class="fa-solid fa-check"></i></button></a>
                </div>

            </div>
        </div>
    </div>

    <div id="ajax_user_manual_desktop"></div>

<!-- SCRIPT FOR LOAD THE INCOMING FOR TODAY -->
<script>
    var url_Incoming_Today = "/Ajax_Incoming/ajax_inc_today.php";
    $(document).ready(function(){
        $('#incoming_today').load(url_Incoming_Today);
    });
</script>

<!-- SCRIPT PARA SA MOBILE MENU -->
<script>
    function openNav() {
        document.getElementById("mySidenav").style.width = "250px";
    }

    function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
    }
</script>

<!-- REPORT / RECEIVING / DOWNLOAD / IMPORT BUTTON -->
<script>

    var urlReport = "Ajax_Report/report.php"
    var urlReport_Table = "Ajax_Report/report_table.php"
    var urlImport_Table = "Ajax_Import/import.php"
    var urlIQC_Data = "/Ajax_IQC/ajax_iqc_data.php"
    var urlEdit_Slip = "/Ajax_Slip_Edit/ajax_slip_edit.php"
    var urlIncoming = "/Ajax_Incoming/ajax_incoming.php"

    // $( document ).ready(function() {
	//     $(".ajax_report").load(urlReport_Table);
    //     $("#report_desktop").addClass("report_download_press");
    // });

    // MOBILE MENU

    $("#receive").click(function(){

        if($("#receive").hasClass("report_download_press")){

            $(".QR-SCANNER-DIV").addClass("close");
            $("#reports").removeClass("close");
            $("#receive").removeClass("report_download_press");

            $(".frame").removeClass("close");

        }

        else{

            $(".QR-SCANNER-DIV").removeClass("close");
            $("#reports").addClass("close");
            $("#receive").addClass("report_download_press");

            $(".frame").addClass("close");

        }

    });

    $("#reports").click(function(){

        if ($("#reports").hasClass("report_download_press")){

            $("#reports").removeClass("report_download_press");
            $("#import").addClass("close");
            $("#import").removeClass("report_download_press");

            // $(".QR-SCANNER-DIV").removeClass("close");

            $(".ajax_report").empty(urlReport);

            $("#report_text").text("Reports");
            $("#report_icon").addClass("fa-chart-simple");
            $("#report_icon").removeClass("fa-chevron-left");

            $("#receive").removeClass("close");

            $(".frame").removeClass("close");

        }

        else{

            $("#reports").addClass("report_download_press");
            $("#import").removeClass("close");

            $(".QR-SCANNER-DIV").addClass("close");

            $(".ajax_report").load(urlReport);

            $(".ajax_report").css({"display": "block"});

            // $("#report_text").text("Back");
            $("#report_icon").removeClass("fa-file-excel");
            $("#report_icon").addClass("fa-chevron-left");

            $("#receive").addClass("close");

            $(".frame").addClass("close");

        }
        
    });

    $("#import").click(function(){

        // $("#import").addClass("report_download_press");

        if($("#import").hasClass("report_download_press")){

            $(".QR-SCANNER-DIV").addClass("close");
            $(".ajax_report").empty(urlReport);

            $("#import").removeClass("report_download_press");
            $("#import").addClass("close");

            $(".ajax_report").css({"display": "flex"});

            $(".frame").removeClass("close");

            $("#receive").removeClass("report_download_press");
            $("#reports").removeClass("report_download_press");
            $("#receive").removeClass("close");
            $("#reports").removeClass("close");

            $("#report_icon").addClass("fa-chart-simple");
            $("#report_icon").removeClass("fa-chevron-left");

        }

        else{

            $(".frame").addClass("close");

            $(".ajax_report").empty(urlReport);
            $(".QR-SCANNER-DIV").addClass("close");

            $(".ajax_report").load(urlImport_Table);
            $(".ajax_report").css({"display": "block"});

            $("#import").addClass("report_download_press");

            $("#receive").removeClass("report_download_press");
            $("#reports").removeClass("report_download_press");
            $("#reports").addClass("close");

        }
    });

    // DESKTOP MENU

    $("#receive_desktop").click(function(){

        if($("#receive_desktop").hasClass("report_download_press")){

            $(".QR-SCANNER-DIV").addClass("close");

            $(".ajax_report").empty(urlReport);

            $("#report_desktop").removeClass("report_download_press");

            $("#receive_desktop").removeClass("report_download_press");
            
            $(".frame").removeClass("close");

            $("#ajax_user_manual_desktop").empty();
            $("#ajax_user_manual_desktop").removeClass("close");

        }

        else{

            $("#ajax_user_manual_desktop").addClass("close");

            $(".QR-SCANNER-DIV").removeClass("close");


            $(".ajax_report").empty(urlReport);

            $("#report_desktop").removeClass("report_download_press");
            $("#import_desktop").removeClass("report_download_press");
            $("#iqc_data").removeClass("report_download_press");
            $("#slip_edit").removeClass("report_download_press");
            $("#to_be_receive_desktop").removeClass("report_download_press");

            $(".import_div").addClass("close");
            
            $("#receive_desktop").addClass("report_download_press");

            $(".frame").addClass("close");

        }

    });

    $("#report_desktop").click(function(){

        if($("#report_desktop").hasClass("report_download_press")){

            $(".QR-SCANNER-DIV").addClass("close");
            $(".ajax_report").empty(urlReport);

            $("#report_desktop").removeClass("report_download_press");
            
            $(".frame").removeClass("close");

            $("#ajax_user_manual_desktop").empty();
            $("#ajax_user_manual_desktop").removeClass("close");

        }

        else{

            $("#ajax_user_manual_desktop").addClass("close");

            $(".QR-SCANNER-DIV").addClass("close");

            $(".ajax_report").load(urlReport_Table);

            $(".ajax_report").empty(urlImport_Table);
            
            $("#report_desktop").addClass("report_download_press");
            $("#receive_desktop").removeClass("report_download_press");
            $("#import_desktop").removeClass("report_download_press");
            $("#iqc_data").removeClass("report_download_press");
            $("#slip_edit").removeClass("report_download_press");
            $("#to_be_receive_desktop").removeClass("report_download_press");

            $(".import_div").addClass("close");
            $(".frame").addClass("close");

            $(".ajax_report").css({"display": "flex"});

        }

    });

    $("#import_desktop").click(function(){

        if($("#import_desktop").hasClass("report_download_press")){

            $(".QR-SCANNER-DIV").addClass("close");
            $(".ajax_report").empty(urlReport);

            $("#import_desktop").removeClass("report_download_press");

            $(".ajax_report").css({"display": "flex"});
            
            $(".frame").removeClass("close");

            $("#ajax_user_manual_desktop").empty();
            $("#ajax_user_manual_desktop").removeClass("close");

        }

        else{

            $("#ajax_user_manual_desktop").addClass("close");
            
            $(".frame").addClass("close");

            $(".ajax_report").empty(urlReport);
            $(".QR-SCANNER-DIV").addClass("close");
            
            $(".ajax_report").load(urlImport_Table);
            $(".ajax_report").css({"display": "block"});
            
            $("#import_desktop").addClass("report_download_press");

            $("#receive_desktop").removeClass("report_download_press");
            $("#report_desktop").removeClass("report_download_press");
            $("#iqc_data").removeClass("report_download_press");
            $("#slip_edit").removeClass("report_download_press");

        }

    });

    $("#iqc_data").click(function(){

        if($("#iqc_data").hasClass("report_download_press")){

            $(".ajax_report").empty(urlIQC_Data);
            
            $(".frame").removeClass("close");

            $("#iqc_data").removeClass("report_download_press");

            $("#iqc_data").removeClass("report_download_press");

        }

        else{

            $("#ajax_user_manual_desktop").addClass("close");

            $(".ajax_report").load(urlIQC_Data);
            
            $(".frame").addClass("close");

            $(".QR-SCANNER-DIV").addClass("close");
            
            $("#iqc_data").addClass("report_download_press");

            $("#receive_desktop").removeClass("report_download_press");
            $("#report_desktop").removeClass("report_download_press");
            $("#import_desktop").removeClass("report_download_press");
            $("#slip_edit").removeClass("report_download_press");
            $("#to_be_receive_desktop").removeClass("report_download_press");

        }

    });

    $("#slip_edit").click(function(){

        if($("#slip_edit").hasClass("report_download_press")){

            $(".ajax_report").empty(urlEdit_Slip);
            
            $(".frame").removeClass("close");

            $("#slip_edit").removeClass("report_download_press");

            $("#slip_edit").removeClass("report_download_press");
            $("#receive_desktop").removeClass("report_download_press");
            $("#report_desktop").removeClass("report_download_press");
            $("#import_desktop").removeClass("report_download_press");
            $("#iqc_data").removeClass("report_download_press");
            $("#to_be_receive_desktop").removeClass("report_download_press");

        }

        else{

            $("#ajax_user_manual_desktop").addClass("close");

            $(".ajax_report").load(urlEdit_Slip);
            
            $(".frame").addClass("close");

            $(".QR-SCANNER-DIV").addClass("close");
            
            $("#slip_edit").addClass("report_download_press");

            $("#receive_desktop").removeClass("report_download_press");
            $("#report_desktop").removeClass("report_download_press");
            $("#import_desktop").removeClass("report_download_press");
            $("#iqc_data").removeClass("report_download_press");
            $("#to_be_receive_desktop").removeClass("report_download_press");

        }

    });

    $("#to_be_receive_desktop").click(function(){

        if($("#to_be_receive_desktop").hasClass("report_download_press")){

            $(".ajax_report").empty(urlEdit_Slip);
            
            $(".frame").removeClass("close");

            $("#slip_edit").removeClass("report_download_press");

            $("#slip_edit").removeClass("report_download_press");
            $("#receive_desktop").removeClass("report_download_press");
            $("#report_desktop").removeClass("report_download_press");
            $("#import_desktop").removeClass("report_download_press");
            $("#iqc_data").removeClass("report_download_press");
            $("#to_be_receive_desktop").removeClass("report_download_press");

        }

        else{

            $("#ajax_user_manual_desktop").addClass("close");

            $(".ajax_report").load(urlIncoming);
            
            $(".frame").addClass("close");

            $(".QR-SCANNER-DIV").addClass("close");
            
            $("#to_be_receive_desktop").addClass("report_download_press");

            $("#receive_desktop").removeClass("report_download_press");
            $("#report_desktop").removeClass("report_download_press");
            $("#import_desktop").removeClass("report_download_press");
            $("#iqc_data").removeClass("report_download_press");

        }

    });
// Traceability QR Code Generator
document.getElementById('print_desktop').addEventListener('click',()=>{
    window.location = "../qrCode_generator/print_qr.php"
})
// Traceability QR Code Generator

</script>

<!-- SCRIPT PARA SA PAG CREATE NG EXCEL FILE -->
<script>    
   

    const monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
    ];

    const d = new Date();
    // document.write("The current month is " + monthNames[d.getMonth()]);

    var Month =  monthNames[d.getMonth()];

    $("#download_desktop").click(function() {

        console.log(Month);

        $("#table_export").table2excel({
            filename: "Table.xls"
        });

    });
   
</script>


<!-- SCRIPT PARA SA IMPORT TABLE FROM EXCEL TO TABLE -->
<script>
    $( document ).ready(function() {
	    $(".export").click(function() {
		    var export_type = $(this).data('export-type');		
		    $('#data_table').tableExport({
                type : export_type,			
                escape : 'false',
                ignoreColumn: []
		    });		
        });
    });
</script>

<!-- SCRIPT FOR USER MANUAL -->
<script>

    var urlUserManualDesktop = "Ajax_User_Manual/ajax_user_desktop.php"

    $("#learn_more_desktop").click(function(){
        $(".frame").addClass("close");

        $("#ajax_user_manual_desktop").load(urlUserManualDesktop);
    });
</script>

<!-- SCRIPT PARA SA MULTIPLE VALUE NG ISANG QR -->
<script>

    $(document).ready(function(){
        $("#count_words").click(function(){
            var words = $.trim($("#QR-Code").val()).split(" ");
            // alert(words.length);
            // var counter = words.length;
            // console.log(counter);
            for (let i = 0; i < words.length; i++) { 
                console.log(words[i]);
            }
        });
    });

</script>

<?php
    echo "<span id='EmpNum' style='display:none;'> $EmpNum </span>";
?>

<script>
    var EmpNum = document.getElementById('EmpNum');
    var EmpNum_value = EmpNum.textContent;

    

    if( EmpNum_value == ' 7113 '){

        $("#iqc_data").removeClass("close");
        $("#slip_edit").removeClass("close");
        
    }
    else if( EmpNum_value == ' 1748 ' ||
             EmpNum_value == ' 8585 ' ||
             EmpNum_value == ' 2668 ' ||
             EmpNum_value == ' 291 ' ||
             EmpNum_value == ' 7527 ' ||
             EmpNum_value == ' 4090 ' ){

        $("#iqc_data").removeClass("close");

    }
    else {

        $("#iqc_data").addClass("close");
        $("#slip_edit").addClass("close");
        
    }
</script>

</body>

</html>

<?php 